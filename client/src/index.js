const express    = require('express');
const app        = express();
const path       = require('path');
const cors       = require('cors')

const LOCAL_HOST = '127.0.0.1';
const PORT       = 2000;

/**
 * folder paths
 */
const gameClientFolderPath = express.static(path.join(__dirname, './game'));
const vendorFolderPath = express.static(path.join(__dirname, './vendor'));

app.use(gameClientFolderPath);
app.use(vendorFolderPath);
app.use(cors());

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/pages/index.html');
});

app.listen(PORT, function() {
  console.log('Frontend server started at ' + LOCAL_HOST + ':' + PORT + '...');
});
