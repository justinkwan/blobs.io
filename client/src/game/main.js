const inputCollector = new InputCollector();
const clientGameUpdater = new ClientGameUpdater();
const objectDrawer = new ObjectDrawer();
var remoteSocketApi = new RemoteSocketApi();


inputCollector.attachKeyListeners();

const socket = remoteSocketApi.getConnection();

initGame();
gameLoop();

async function gameLoop() {

  //const playerServerJson = await remoteSocketApi.onServerPlayerUpdate();

  socket.on('server_player_state', function(playerServerJson) {
    playerServerJson = JSON.parse(playerServerJson);

    // check for deviation of server and client pos
    // then correct if out of range
  });


  if(clientGameUpdater.player.areKeysPressed()) {
    remoteSocketApi.emitPlayerInput();
    console.log("done emitting player input!");
  }


  console.log("next part!");
  clientGameUpdater.updatePlayerPos();


  const player = clientGameUpdater.getPlayer();

  objectDrawer.drawPlayer(player);


  // only do this in the correction function if client deviates too much

  objectDrawer.renderDrawing();

  window.requestAnimationFrame(gameLoop);
}


function initGame() {
  objectDrawer.init();
  //clientGameUpdater.initPlayer();
}
