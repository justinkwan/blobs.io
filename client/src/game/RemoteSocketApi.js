class RemoteSocketApi {

  constructor() {

  }

  getConnection() {
    var socket = io.connect('http://localhost:2200');
    socket.emit('connection', '');
    socket.emit('client_new_player', '');
    return socket;
  }

  emitPlayerInput() {
    socket.emit('client_player_state', JSON.stringify({
      "upKey": clientGameUpdater.player.isUpKeyPressed(),
      "downKey": clientGameUpdater.player.isDownKeyPressed(),
      "leftKey": clientGameUpdater.player.isLeftKeyPressed(),
      "rightKey": clientGameUpdater.player.isRightKeyPressed()
    }));
  }

  onServerPlayerUpdate() {
    var promise = new Promise(function(resolve, reject) {
      socket.on('server_player_state', function(playerServerJson) {
        playerServerJson = JSON.parse(playerServerJson);
        resolve(playerServerJson);
      });
    });

    return promise;
  }

}
