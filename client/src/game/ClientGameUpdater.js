
class ClientGameUpdater {

  constructor() {
    this.player = new Player("id");
  }

  initPlayer() {


    


    // this.player.setXPos(150);
    // this.player.setYPos(150);
    // this.player.setXSpeed(6);
    // this.player.setYSpeed(3);
  }

  updatePlayerPos() {
    if(this.player.isUpKeyPressed()) {
      this.player.moveUp();
    }
    if(this.player.isDownKeyPressed()) {
      this.player.moveDown();
    }
    if(this.player.isLeftKeyPressed()) {
      this.player.moveLeft();
    }
    if(this.player.isRightKeyPressed()) {
      this.player.moveRight();
    }
    console.log("player x: " + this.player.getXPos() + " player y: " + this.player.getYPos());
  }

  getPlayer() {
    return this.player;
  }

}
