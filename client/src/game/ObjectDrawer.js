/**
 * Draws objects and adds them to the stage to return
 */

class ObjectDrawer {

  constructor() {
    this.stage = new PIXI.Container();
    this.renderer = new PIXI.CanvasRenderer(3000, 3000); // should be viewport
    this.circle = new PIXI.Graphics();
  }

  init() {
    const windowSize = {
      width: $(window).height(),
      height: $(window).width()
    };
    this.circle.beginFill(0xFF00FF);
    this.circle.drawCircle(clientGameUpdater.player.getXPos(), clientGameUpdater.player.getYPos(), 30);
    this.stage.addChild(this.circle);
    document.body.appendChild(this.renderer.view);
  }

  drawMap() {

  }

  drawPlayer(player) {
    this.circle.x = clientGameUpdater.player.getXPos();
    this.circle.y = clientGameUpdater.player.getYPos();
  }

  renderDrawing() {
    this.renderer.render(this.stage);
  }



}
