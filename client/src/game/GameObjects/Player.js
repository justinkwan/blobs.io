
class Player extends GameObject {

  constructor(id, xCenter, yCenter, xSpeed, ySpeed) {
    super(id, xCenter, yCenter, xSpeed, ySpeed);
    this.FULL_HEALTH = 100;
    this.HEALTH_INCREMENT = 15;
    this.HEALTH_DECREMENT = 15;

    this.INITIAL_SCORE = 0;
    this.SCORE_INCREMENT = 150;

    this.score = this.INITIAL_SCORE;
    this.health = this.FULL_HEALTH;
    this.bullets = [];

    this.upKey = false
    this.downKey = false;
    this.leftKey = false;
    this.rightKey = false;
  }

  getHealth() {
    return this.health;
  }

  isUpKeyPressed() {
    return this.upKey;
  }

  isDownKeyPressed() {
    return this.downKey;
  }

  isLeftKeyPressed() {
    return this.leftKey;
  }

  isRightKeyPressed() {
    return this.rightKey;
  }

  areKeysPressed() {
    return this.upKey || this.downKey || this.leftKey || this.rightKey;
  }

  decreaseHealth() {
    this.health -= this.HEALTH_DECREMENT;
  }

  increaseHealth() {
    this.health += this.HEALTH_INCREMENT;
  }

  resetHealth() {
    this.health = this.FULL_HEALTH;
  }

  increaseScore() {
    this.score += this.SCORE_INCREMENT;
  }

  resetScore() {
    this.score = this.INITIAL_SCORE;
  }

}
