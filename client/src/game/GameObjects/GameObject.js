class GameObject {

  constructor(id, xCenter, yCenter, xSpeed, ySpeed) {
    this.id = id;
    this.xCenter = xCenter;
    this.yCenter = yCenter;
    this.xSpeed  = xSpeed;
    this.ySpeed  = ySpeed;
    this.radius = 20;
    this.isInvisible = false;
  }

  getId() {
    return this.id;
  }

  moveRight() {
    this.xCenter += this.xSpeed;
  }

  moveLeft() {
    this.xCenter -= this.xSpeed;
  }

  moveUp() {
    this.yCenter -= this.ySpeed;
  }

  moveDown() {
    this.yCenter += this.ySpeed;
  }

  setXPos(newXCenter) {
    this.xCenter = newXCenter;
  }

  setYPos(newYCenter) {
    this.yCenter = newYCenter;
  }

  getXPos() {
    return this.xCenter;
  }

  getYPos() {
    return this.yCenter;
  }

  setXSpeed(newXSpeed) {
    this.xSpeed = newXSpeed;
  }

  setYSpeed(newYSpeed) {
    this.ySpeed = newYSpeed;
  }

  // get edge functions
  getLeftEdge() {

  }

  getRightEdge() {

  }

  getTopEdge() {

  }

  getBottomEdge() {

  }

  die() {
    this.isInvisible = true;
  }

  isDead() {
    return this.isInvisible;
  }

  resetLife() {
    this.isInvisible = false;
  }


}
