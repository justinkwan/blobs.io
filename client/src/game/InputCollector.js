

class InputCollector {

  constructor() {

  }

  onKeyPress(event) {
    if(event.keyCode === 87) clientGameUpdater.player.upKey = true;
    if(event.keyCode === 83) clientGameUpdater.player.downKey = true;
    if(event.keyCode === 65) clientGameUpdater.player.leftKey = true;
    if(event.keyCode === 68) clientGameUpdater.player.rightKey = true;
  }

  onKeyRelease(event) {
    if(event.keyCode === 87) clientGameUpdater.player.upKey = false;
    if(event.keyCode === 83) clientGameUpdater.player.downKey = false;
    if(event.keyCode === 65) clientGameUpdater.player.leftKey = false;
    if(event.keyCode === 68) clientGameUpdater.player.rightKey = false;
  }


  attachKeyListeners() {
    window.addEventListener('keydown', this.onKeyPress);
    window.addEventListener('keyup', this.onKeyRelease);
  }

  detachKeyListeners() {
    window.removeEventListener('keydown', this.onKeyPress);
    window.removeEventListener('keyup', this.onKeyRelease);
  }


}
