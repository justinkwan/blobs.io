const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const cors = require('cors');
const GameUpdater = require('./GameUpdater.js');

server.listen(2200);

app.use(cors());
app.options('http://127.0.0.1:2000', cors());

var gameUpdater = new GameUpdater()


io.on('connection', function(socket) {

  socket.emit('news', "connected to socket server!");



  socket.on('client_new_player', function(emptyData) {
    console.log("new player: " + socket.id + " joined!");
    gameUpdater.handleNewPlayer(socket.id);
    const player = gameUpdater.getUpdatedPlayer(socket.id);
    socket.emit("server_player_state", player);
  });



  socket.on('client_player_state', function(playerClientJson) {

    //console.log("updating player: " + socket.id);

    if(gameUpdater.gameRoom.doesPlayerExist(socket.id) === false) {
      console.log("Socket DNE!");
      socket.emit("server_player_state", JSON.stringify("error, player dne"));
      return;
    }

    gameUpdater.updatePlayer(socket.id, playerClientJson);
    const player = gameUpdater.getUpdatedPlayer(socket.id);
    socket.emit("server_player_state", player);
  });

});

io.on('disconnect', function(socket) {

  socket.emit('news', {
    hello: 'world'
  });

  socket.on('my other event', function(data) {
    console.log(data);
  });

});
