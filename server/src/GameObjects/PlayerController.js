/**
 * updates player attributes and states based on player data from client
 * parsing through the json from request
 */


class PlayerController {

  constructor() {

  }

  updatePos(playerClientJson, player) {
    if(playerClientJson.upKey)
      player.moveUp();
    if(playerClientJson.downKey)
      player.moveDown();
    if(playerClientJson.leftKey)
      player.moveLeft();
    if(playerClientJson.rightKey)
      player.moveRight();
  }

}

module.exports = PlayerController;
