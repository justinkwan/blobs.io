class GameRoom {

  constructor() {
    this.players = [];
  }

  addPlayer(newPlayer) {
    this.players.push(newPlayer);
  }

  removePlayer(playerId) {
    for(var i = 0; i < this.getNumOfPlayers(); i++) {
      if(this.players[i].getId() === playerId) {
        this.players.splice(i, 1);
      }
    }
  }

  replacePlayer(playerId, player) {
    for(var i = 0; i < this.getNumOfPlayers(); i++) {
      if(this.players[i].getId() === playerId) {
        this.players[i] = player;
        break;
      }
    }
  }

  getPlayer(playerId) {
    for(var i = 0; i < this.getNumOfPlayers(); i++) {
      if(this.players[i].getId() === playerId) {
        return this.players[i];
      }
    }
  }

  getAllPlayers() {
    return this.players;
  }

  getNumOfPlayers() {
    return this.players.length;
  }

  doesPlayerExist(playerId) {
    for(var i = 0; i < this.getNumOfPlayers(); i++) {
      if(this.players[i].getId() === playerId) {
        return true;
      }
    }
    return false;
  }

  clear() {
    this.players = [];
  }


}

module.exports = GameRoom;
