const Player = require('./GameObjects/Player.js');
const PlayerController = require('./GameObjects/PlayerController.js');
const GameRoom = require('./GameObjects/GameRoom.js');



class GameUpdater {

  constructor() {
    this.gameRoom = new GameRoom();
    this.playerController = new PlayerController();
  }

  handleNewPlayer(playerId) {
    const player = new Player(playerId, 0, 0, 6, 3);
    this.gameRoom.addPlayer(player);
  }

  updatePlayer(playerId, playerClientJson) {
    playerClientJson = JSON.parse(playerClientJson);
    const player = this.gameRoom.getPlayer(playerId);

    console.log("player x: " + JSON.stringify(player.xCenter) + " player y: " + JSON.stringify(player.yCenter));

    // TODO: check for collisions here

    this.playerController.updatePos(playerClientJson, player);
    this.gameRoom.replacePlayer(playerId, player);
  }

  getUpdatedPlayer(playerId) {
    const player = this.gameRoom.getPlayer(playerId);
    return JSON.stringify(player);
  }

}

module.exports = GameUpdater;
