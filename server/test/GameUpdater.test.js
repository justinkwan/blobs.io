const expect = require('chai').expect;
const assert = require('assert');
const GameUpdater = require('../src/GameUpdater.js');
const GameRoom = require('../src/GameObjects/GameRoom.js');
const Player = require('../src/GameObjects/Player.js');

var gameUpdater = new GameUpdater();
var gameRoom = new GameRoom();
var player = new Player("id_1", 0, 0, 0, 0);

describe('Game Updater Tests', function() {

  beforeEach(function(done) {
    gameUpdater.gameRoom.clear();
    done();
  });

  it("handleNewPlayer() - should add new player to gameRoom", function() {
    gameUpdater.handleNewPlayer("id_1");
    const player = gameUpdater.getUpdatedPlayer("id_1");
    assert.equal(player, JSON.stringify({
      "id": "id_1",
      "xCenter": 150,
      "yCenter": 150,
      "xSpeed": 6,
      "ySpeed": 3,
      "radius": 20,
      "isInvisible": false,
      "FULL_HEALTH": 100,
      "HEALTH_INCREMENT": 15,
      "HEALTH_DECREMENT": 15,
      "INITIAL_SCORE": 0,
      "SCORE_INCREMENT": 150,
      "score": 0,
      "health": 100,
      "bullets": []
    }));
  });

  it("handleNewPlayer() - should add new player to gameRoom", function() {
    gameUpdater.handleNewPlayer("id_2");
    const player = gameUpdater.getUpdatedPlayer("id_2");
    assert.equal(player, JSON.stringify({
      "id": "id_2",
      "xCenter": 150,
      "yCenter": 150,
      "xSpeed": 6,
      "ySpeed": 3,
      "radius": 20,
      "isInvisible": false,
      "FULL_HEALTH": 100,
      "HEALTH_INCREMENT": 15,
      "HEALTH_DECREMENT": 15,
      "INITIAL_SCORE": 0,
      "SCORE_INCREMENT": 150,
      "score": 0,
      "health": 100,
      "bullets": []
    }));
  });

  it("getUpdatedPlayer() - gets updated player with id: 'id_231'", function() {
    gameUpdater.handleNewPlayer("id_231");
    const player = gameUpdater.getUpdatedPlayer("id_231");
    assert.equal(player, JSON.stringify({
      "id": "id_231",
      "xCenter": 150,
      "yCenter": 150,
      "xSpeed": 6,
      "ySpeed": 3,
      "radius": 20,
      "isInvisible": false,
      "FULL_HEALTH": 100,
      "HEALTH_INCREMENT": 15,
      "HEALTH_DECREMENT": 15,
      "INITIAL_SCORE": 0,
      "SCORE_INCREMENT": 150,
      "score": 0,
      "health": 100,
      "bullets": []
    }));
  });

  it("getUpdatedPlayer() - gets updated player with id: 'id_12'", function() {
    gameUpdater.handleNewPlayer("id_12");
    const player = gameUpdater.getUpdatedPlayer("id_12");
    assert.equal(player, JSON.stringify({
      "id": "id_12",
      "xCenter": 150,
      "yCenter": 150,
      "xSpeed": 6,
      "ySpeed": 3,
      "radius": 20,
      "isInvisible": false,
      "FULL_HEALTH": 100,
      "HEALTH_INCREMENT": 15,
      "HEALTH_DECREMENT": 15,
      "INITIAL_SCORE": 0,
      "SCORE_INCREMENT": 150,
      "score": 0,
      "health": 100,
      "bullets": []
    }));
  });



});
