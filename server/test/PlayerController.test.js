const expect = require('chai').expect;
const assert = require('assert');
const Player = require('../src/GameObjects/Player.js');
const PlayerController = require('../src/GameObjects/PlayerController.js');

var playerController = new PlayerController();
var player = new Player("id_1", 100, 200, 3, 4);

describe('PlayerController Tests', function() {

  /**
   * resetting the object x and y position/speed before every test
   */
  beforeEach(function(done) {
    player.setXPos(100);
    player.setYPos(200);
    done();
  });

  // no keystroke case
  it("updatePos() - should move player up from client JSON", function() {
    const playerClientJson = getPlayerClientJson(false, false, false, false);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 100);
  });

  // single keystroke movement cases
  it("updatePos() - should move player up from client JSON", function() {
    const playerClientJson = getPlayerClientJson(true, false, false, false);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 196);
    assert.equal(player.xCenter, 100);
  });

  it("updatePos() - should move player down from client JSON", function() {
    const playerClientJson = getPlayerClientJson(false, true, false, false);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 204);
    assert.equal(player.xCenter, 100);
  });

  it("updatePos() - should move player left from client JSON", function() {
    const playerClientJson = getPlayerClientJson(false, false, true, false);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 97);
  });

  it("updatePos() - should move player right from client JSON", function() {
    const playerClientJson = getPlayerClientJson(false, false, false, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 103);
  });

  // multi-keystroke movement cases
  it("updatePos() - should move player up & right from client JSON", function() {
    const playerClientJson = getPlayerClientJson(true, false, false, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 196);
    assert.equal(player.xCenter, 103);
  });

  it("updatePos() - should move player up & left from client JSON", function() {
    const playerClientJson = getPlayerClientJson(true, false, true, false);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 196);
    assert.equal(player.xCenter, 97);
  });

  it("updatePos() - should move player down & right from client JSON", function() {
    const playerClientJson = getPlayerClientJson(false, true, false, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 204);
    assert.equal(player.xCenter, 103);
  });

  it("updatePos() - should move player down & left from client JSON", function() {
    const playerClientJson = getPlayerClientJson(false, true, true, false);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 204);
    assert.equal(player.xCenter, 97);
  });

  // three keystroke movement cases
  it("updatePos() - should move player left from client JSON", function() {
    const playerClientJson = getPlayerClientJson(true, true, true, false);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 97);
  });

  it("updatePos() - should move player right from client JSON", function() {
    const playerClientJson = getPlayerClientJson(true, true, false, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 103);
  });

  it("updatePos() - should move player up from client JSON", function() {
    const playerClientJson = getPlayerClientJson(true, false, true, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 196);
    assert.equal(player.xCenter, 100);
  });

  it("updatePos() - should move player down from client JSON", function() {
    const playerClientJson = getPlayerClientJson(false, true, true, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 204);
    assert.equal(player.xCenter, 100);
  });

  // multi-keystroke no movement cases
  it("updatePos() - should not move player from client JSON (up & down key pressed)", function() {
    const playerClientJson = getPlayerClientJson(true, true, false, false);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 100);
  });

  it("updatePos() - should not move player from client JSON (left & right key pressed)", function() {
    const playerClientJson = getPlayerClientJson(false, false, true, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 100);
  });

  it("updatePos() - should not move player from client JSON (left & right key pressed)", function() {
    const playerClientJson = getPlayerClientJson(false, false, true, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 100);
  });

  it("updatePos() - should not move player from client JSON (all keys pressed)", function() {
    const playerClientJson = getPlayerClientJson(true, true, true, true);
    playerController.updatePos(playerClientJson, player);
    assert.equal(player.yCenter, 200);
    assert.equal(player.xCenter, 100);
  });


});


function getPlayerClientJson(upKeyValue, downKeyValue, leftKeyValue, rightKeyValue) {
  return {
    "id": "id_1",
    "xCenter": 100,
    "yCenter": 200,
    "xSpeed": 3,
    "ySpeed": 4,
    "radius": 20,
    "isInvisible": false,
    "FULL_HEALTH": 100,
    "HEALTH_INCREMENT": 15,
    "HEALTH_DECREMENT": 15,
    "INITIAL_SCORE": 0,
    "SCORE_INCREMENT": 150,
    "score": 0,
    "health": 100,
    "bullets": [],
    "upKey": upKeyValue,
    "downKey": downKeyValue,
    "leftKey": leftKeyValue,
    "rightKey": rightKeyValue
  };
}
