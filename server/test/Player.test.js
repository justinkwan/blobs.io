const expect = require('chai').expect;
const assert = require('assert');
const Player = require('../src/GameObjects/Player.js');

var player = new Player("id_player", 40, 50, 5, 5);

describe('Player Tests', function() {

  /**
   * resetting the object x and y position/speed before every test
   */
  beforeEach(function(done) {
    player.resetHealth();
    player.resetScore();
    done();
  });

  it("getHealth() - player's health is 100%", function() {
    const health = player.getHealth();
    assert.equal(health, 100);
  });

  it("getHealth() - player's health is 100%", function() {
    player.decreaseHealth();
    const health = player.getHealth();
    assert.equal(health, 85);
  });

  it("decreaseHealth() - player's health is decreased to 85%", function() {
    player.decreaseHealth();
    assert.equal(player.health, 85);
  });

  it("decreaseHealth() - player's health is decreased to 55%", function() {
    decreaseHealthNTimes(3);
    assert.equal(player.health, 55);
  });

  it("increaseHealth() - player's health is increased to 115%", function() {
    player.increaseHealth();
    assert.equal(player.health, 115);
  });

  it("increaseHealth() - player's health is increased to 145%", function() {
    increaseHealthNTimes(3);
    assert.equal(player.health, 145);
  });

  it("resetHealth() - player's health is reset to 100%", function() {
    player.decreaseHealth();
    player.resetHealth();
    assert.equal(player.health, 100);
  });

  it("resetHealth() - player's health is reset to 100%", function() {
    increaseHealthNTimes(3);
    player.resetHealth();
    assert.equal(player.health, 100);
  });

  it("resetHealth() - player's health is reset to 100%", function() {
    decreaseHealthNTimes(45);
    player.resetHealth();
    assert.equal(player.health, 100);
  });

  it("increaseScore() - player's score is increased to 150", function() {
    player.increaseScore();
    assert.equal(player.score, 150);
  });

  it("increaseScore() - player's score is increased to 1650", function() {
    increaseScoreNTimes(11);
    assert.equal(player.score, 1650);
  });

  it("resetScore() - player's score is reset to 0 from 150", function() {
    player.increaseScore();
    assert.equal(player.score, 150);
    player.resetScore();
    assert.equal(player.score, 0);
  });

  it("resetScore() - player's score is reset to 0 from 1650", function() {
    increaseScoreNTimes(11);
    assert.equal(player.score, 1650);
    player.resetScore();
    assert.equal(player.score, 0);
  });

});

function increaseHealthNTimes(numOfIncreases) {
  for (var i = 0; i < numOfIncreases; i++) {
    player.increaseHealth();
  }
}

function decreaseHealthNTimes(numOfDecreases) {
  for (var i = 0; i < numOfDecreases; i++) {
    player.decreaseHealth();
  }
}

function increaseScoreNTimes(numOfIncreases) {
  for (var i = 0; i < numOfIncreases; i++) {
    player.increaseScore();
  }
}
