const expect = require('chai').expect;
const assert = require('assert');
const GameObject = require('../src/GameObjects/GameObject.js');

var gameObject = new GameObject("id", 30, 40, 3, 3);

describe('GameObject Tests', function() {

  /**
   * resetting the object x and y position/speed before every test
   */
  beforeEach(function(done) {
    gameObject.setXPos(30);
    gameObject.setYPos(40);
    gameObject.setXSpeed(3);
    gameObject.setYSpeed(3);
    gameObject.resetLife();
    done();
  });

  it('getId() - get id "id"', function() {
    id = gameObject.getId();
    assert.equal(id, "id");
  });

  it('moveRight() - update xCenter from 30 to 33', function() {
    gameObject.moveRight();
    assert.equal(gameObject.xCenter, 33);
  });

  it('moveLeft() - update xCenter from 30 to 27', function() {
    gameObject.moveLeft();
    assert.equal(gameObject.xCenter, 27);
  });

  it('moveUp() - update yCenter from 40 to 37', function() {
    gameObject.moveUp();
    assert.equal(gameObject.yCenter, 37);
  });

  it('moveDown() - update yCenter from 40 to 37', function() {
    gameObject.moveDown();
    assert.equal(gameObject.yCenter, 43);
  });

  it('setXPos() - update xCenter from 30 to 6', function() {
    gameObject.setXPos(6);
    assert.equal(gameObject.xCenter, 6);
  });

  it('setXPos() - update xCenter from 30 to -60', function() {
    gameObject.setXPos(-60);
    assert.equal(gameObject.xCenter, -60);
  });

  it('setYPos() - update yCenter from 40 to -50', function() {
    gameObject.setYPos(-50);
    assert.equal(gameObject.yCenter, -50);
  });

  it('setYPos() - update yCenter from 40 to 2000', function() {
    gameObject.setYPos(2000);
    assert.equal(gameObject.yCenter, 2000);
  });

  it('getXPos() - should get xPos of 2000', function() {
    gameObject.setXPos(2000);
    const xPos = gameObject.getXPos();
    assert.equal(xPos, 2000);
  });

  it('getYPos() - should get yPos of 2001', function() {
    gameObject.setYPos(2001);
    const yPos = gameObject.getYPos();
    assert.equal(yPos, 2001);
  });

  it('setXSpeed() - update xSpeed from 3 to 21', function() {
    gameObject.setXSpeed(21);
    assert.equal(gameObject.xSpeed, 21);
  });

  it('setXSpeed() - update xSpeed from 3 to -2.321', function() {
    gameObject.setXSpeed(-2.321);
    assert.equal(gameObject.xSpeed, -2.321);
  });

  it('setYSpeed() - update ySpeed from 3 to 1.23', function() {
    gameObject.setYSpeed(1.23);
    assert.equal(gameObject.ySpeed, 1.23);
  });

  it('setYSpeed() - update ySpeed from 3 to 2312.312312', function() {
    gameObject.setYSpeed(2312.312312);
    assert.equal(gameObject.ySpeed, 2312.312312);
  });

  it("die() - gameObject state isInvisible = true", function() {
    // ensure gameObject is alive before calling die()
    assert.equal(gameObject.isInvisible, false);
    gameObject.die();
    assert.equal(gameObject.isInvisible, true);
  });

  it("isDead() - get gameObject isInvisible state which is true", function() {
    const isDead = gameObject.isDead();
    assert.equal(isDead, false);
  });

  it("isDead() - get gameObject isInvisible state which is false", function() {
    gameObject.die();
    const isDead = gameObject.isDead();
    assert.equal(isDead, true);
  });




});
