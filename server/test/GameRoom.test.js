const expect = require('chai').expect;
const assert = require('assert');
const GameRoom = require('../src/GameObjects/GameRoom.js');
const Player = require('../src/GameObjects/Player.js');

var gameRoom = new GameRoom();

/**
 * TODO: Change all test inputs for lists to Player Objects later
 *       then assert on the Player objects and it's attributes
 */
describe('GameRoom Tests', function() {

  /**
   * custom setup before every test (it)
   */
  beforeEach(function(done) {
    gameRoom.clear();
    done();
  });

  it('addPlayer() - gameRoom.players[0] should be the added player', function() {
    var player = new Player("id_123", 312, 412, 3, 8);
    gameRoom.addPlayer(player);
    assert.equal(gameRoom.players.length, 1);
    assert.deepEqual(JSON.stringify(gameRoom.players[0]),
      JSON.stringify({
        "id": "id_123",
        "xCenter": 312,
        "yCenter": 412,
        "xSpeed": 3,
        "ySpeed": 8,
        "radius": 20,
        "isInvisible": false,
        "FULL_HEALTH": 100,
        "HEALTH_INCREMENT": 15,
        "HEALTH_DECREMENT": 15,
        "INITIAL_SCORE": 0,
        "SCORE_INCREMENT": 150,
        "score": 0,
        "health": 100,
        "bullets": []
      }));
  });


  it('addPlayer() - gameRoom.players should be the 4 added players', function() {
    var player = new Player("id_123", 312, 412, 3, 8);
    addTestPlayersToGameRoom();
    assert.equal(gameRoom.players.length, 4);
    assert.deepEqual(JSON.stringify(gameRoom.players),
      JSON.stringify([{
          "id": "id_1",
          "xCenter": 300,
          "yCenter": 400,
          "xSpeed": 3,
          "ySpeed": 4,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_2",
          "xCenter": 400,
          "yCenter": 500,
          "xSpeed": 4,
          "ySpeed": 5,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_3",
          "xCenter": 500,
          "yCenter": 600,
          "xSpeed": 5,
          "ySpeed": 6,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_4",
          "xCenter": 600,
          "yCenter": 700,
          "xSpeed": 6,
          "ySpeed": 7,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        }
      ]));
  });

  it('removePlayer() - gameRoom.players should be empty', function() {
    var player = new Player("id_123", 312, 412, 3, 8);
    gameRoom.addPlayer(player);
    assert.equal(gameRoom.players.length, 1);
    gameRoom.removePlayer("id_123");
    assert.deepEqual(gameRoom.players, []);
  });

  it('removePlayer() - player with id: "id_1" should be removed', function() {
    var player = new Player("id_123", 312, 412, 3, 8);
    addTestPlayersToGameRoom();
    assert.equal(gameRoom.players.length, 4);
    gameRoom.removePlayer("id_1");
    assert.deepEqual(JSON.stringify(gameRoom.players),
      JSON.stringify([{
          "id": "id_2",
          "xCenter": 400,
          "yCenter": 500,
          "xSpeed": 4,
          "ySpeed": 5,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_3",
          "xCenter": 500,
          "yCenter": 600,
          "xSpeed": 5,
          "ySpeed": 6,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_4",
          "xCenter": 600,
          "yCenter": 700,
          "xSpeed": 6,
          "ySpeed": 7,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        }
      ]));
  });

  it('removePlayer() - player with id: "id_3" should be removed', function() {
    var player = new Player("id_123", 312, 412, 3, 8);
    addTestPlayersToGameRoom();
    assert.equal(gameRoom.players.length, 4);
    gameRoom.removePlayer("id_3");
    assert.deepEqual(JSON.stringify(gameRoom.players),
      JSON.stringify([{
          "id": "id_1",
          "xCenter": 300,
          "yCenter": 400,
          "xSpeed": 3,
          "ySpeed": 4,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_2",
          "xCenter": 400,
          "yCenter": 500,
          "xSpeed": 4,
          "ySpeed": 5,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_4",
          "xCenter": 600,
          "yCenter": 700,
          "xSpeed": 6,
          "ySpeed": 7,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        }
      ]));
  });

  it('removePlayer() - player with id: "id_4" should be removed', function() {
    var player = new Player("id_123", 312, 412, 3, 8);
    addTestPlayersToGameRoom();
    assert.equal(gameRoom.players.length, 4);
    gameRoom.removePlayer("id_4");
    assert.deepEqual(JSON.stringify(gameRoom.players),
      JSON.stringify([{
          "id": "id_1",
          "xCenter": 300,
          "yCenter": 400,
          "xSpeed": 3,
          "ySpeed": 4,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_2",
          "xCenter": 400,
          "yCenter": 500,
          "xSpeed": 4,
          "ySpeed": 5,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_3",
          "xCenter": 500,
          "yCenter": 600,
          "xSpeed": 5,
          "ySpeed": 6,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        }
      ]));
  });

  it('replacePlayer() - replace player with id: "id_1"', function() {
    addTestPlayersToGameRoom();
    var player = new Player("id_1", 0, 0, 0, 0);
    gameRoom.replacePlayer("id_1", player);
    assert.deepEqual(JSON.stringify(gameRoom.players),
      JSON.stringify([{
          "id": "id_1",
          "xCenter": 0,
          "yCenter": 0,
          "xSpeed": 0,
          "ySpeed": 0,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_2",
          "xCenter": 400,
          "yCenter": 500,
          "xSpeed": 4,
          "ySpeed": 5,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_3",
          "xCenter": 500,
          "yCenter": 600,
          "xSpeed": 5,
          "ySpeed": 6,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_4",
          "xCenter": 600,
          "yCenter": 700,
          "xSpeed": 6,
          "ySpeed": 7,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        }
      ]));
  });


  it('replacePlayer() - replace player with id: "id_3"', function() {
    addTestPlayersToGameRoom();
    var player = new Player("id_3", 3, 4, 7, 8);
    gameRoom.replacePlayer("id_3", player);
    assert.deepEqual(JSON.stringify(gameRoom.players),
      JSON.stringify([{
          "id": "id_1",
          "xCenter": 300,
          "yCenter": 400,
          "xSpeed": 3,
          "ySpeed": 4,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_2",
          "xCenter": 400,
          "yCenter": 500,
          "xSpeed": 4,
          "ySpeed": 5,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_3",
          "xCenter": 3,
          "yCenter": 4,
          "xSpeed": 7,
          "ySpeed": 8,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_4",
          "xCenter": 600,
          "yCenter": 700,
          "xSpeed": 6,
          "ySpeed": 7,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        }
      ]));
  });

  it('replacePlayer() - replace player with id: "id_4"', function() {
    addTestPlayersToGameRoom();
    var player = new Player("id_4", 0, 0, 0, 0);
    gameRoom.replacePlayer("id_4", player);
    assert.deepEqual(JSON.stringify(gameRoom.players),
      JSON.stringify([{
          "id": "id_1",
          "xCenter": 300,
          "yCenter": 400,
          "xSpeed": 3,
          "ySpeed": 4,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_2",
          "xCenter": 400,
          "yCenter": 500,
          "xSpeed": 4,
          "ySpeed": 5,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_3",
          "xCenter": 500,
          "yCenter": 600,
          "xSpeed": 5,
          "ySpeed": 6,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_4",
          "xCenter": 0,
          "yCenter": 0,
          "xSpeed": 0,
          "ySpeed": 0,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        }
      ]));
  });

  it('getPlayer() - player with id: "id_1" should be retrieved', function() {
    addTestPlayersToGameRoom();
    const player = gameRoom.getPlayer("id_1");
    assert.deepEqual(JSON.stringify(player),
      JSON.stringify({
        "id": "id_1",
        "xCenter": 300,
        "yCenter": 400,
        "xSpeed": 3,
        "ySpeed": 4,
        "radius": 20,
        "isInvisible": false,
        "FULL_HEALTH": 100,
        "HEALTH_INCREMENT": 15,
        "HEALTH_DECREMENT": 15,
        "INITIAL_SCORE": 0,
        "SCORE_INCREMENT": 150,
        "score": 0,
        "health": 100,
        "bullets": []
      }));
  });

  it('getPlayer() - player with id: "id_2" should be retrieved', function() {
    addTestPlayersToGameRoom();
    const player = gameRoom.getPlayer("id_2");
    assert.deepEqual(JSON.stringify(player),
      JSON.stringify({
        "id": "id_2",
        "xCenter": 400,
        "yCenter": 500,
        "xSpeed": 4,
        "ySpeed": 5,
        "radius": 20,
        "isInvisible": false,
        "FULL_HEALTH": 100,
        "HEALTH_INCREMENT": 15,
        "HEALTH_DECREMENT": 15,
        "INITIAL_SCORE": 0,
        "SCORE_INCREMENT": 150,
        "score": 0,
        "health": 100,
        "bullets": []
      }));
  });

  it('getPlayer() - player with id: "id_4" should be retrieved', function() {
    addTestPlayersToGameRoom();
    const player = gameRoom.getPlayer("id_4");
    assert.deepEqual(JSON.stringify(player),
      JSON.stringify({
        "id": "id_4",
        "xCenter": 600,
        "yCenter": 700,
        "xSpeed": 6,
        "ySpeed": 7,
        "radius": 20,
        "isInvisible": false,
        "FULL_HEALTH": 100,
        "HEALTH_INCREMENT": 15,
        "HEALTH_DECREMENT": 15,
        "INITIAL_SCORE": 0,
        "SCORE_INCREMENT": 150,
        "score": 0,
        "health": 100,
        "bullets": []
      }));
  });

  it('getAllPlayers() - should get no players (empty)', function() {
    const players = gameRoom.getAllPlayers();
    assert.deepEqual(players, []);
  });

  it('getAllPlayers() - should get the 1 player', function() {
    var player = new Player("id_09", 0, 4.231, 5, -8.22);
    gameRoom.addPlayer(player);
    const players = gameRoom.getAllPlayers();
    assert.deepEqual(JSON.stringify(players),
      JSON.stringify([{
        "id": "id_09",
        "xCenter": 0,
        "yCenter": 4.231,
        "xSpeed": 5,
        "ySpeed": -8.22,
        "radius": 20,
        "isInvisible": false,
        "FULL_HEALTH": 100,
        "HEALTH_INCREMENT": 15,
        "HEALTH_DECREMENT": 15,
        "INITIAL_SCORE": 0,
        "SCORE_INCREMENT": 150,
        "score": 0,
        "health": 100,
        "bullets": []
      }]));
  });

  it('getAllPlayers() - should get all 4 players', function() {
    addTestPlayersToGameRoom();
    const players = gameRoom.getAllPlayers();
    assert.deepEqual(JSON.stringify(players),
      JSON.stringify([{
          "id": "id_1",
          "xCenter": 300,
          "yCenter": 400,
          "xSpeed": 3,
          "ySpeed": 4,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_2",
          "xCenter": 400,
          "yCenter": 500,
          "xSpeed": 4,
          "ySpeed": 5,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_3",
          "xCenter": 500,
          "yCenter": 600,
          "xSpeed": 5,
          "ySpeed": 6,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        },
        {
          "id": "id_4",
          "xCenter": 600,
          "yCenter": 700,
          "xSpeed": 6,
          "ySpeed": 7,
          "radius": 20,
          "isInvisible": false,
          "FULL_HEALTH": 100,
          "HEALTH_INCREMENT": 15,
          "HEALTH_DECREMENT": 15,
          "INITIAL_SCORE": 0,
          "SCORE_INCREMENT": 150,
          "score": 0,
          "health": 100,
          "bullets": []
        }
      ]));
  });

  it('getNumOfPlayers() - should get gameRoom players length of 0', function() {
    const numOfPlayers = gameRoom.getNumOfPlayers();
    assert.equal(numOfPlayers, 0);
  });

  it('getNumOfPlayers() - should get gameRoom players length of 1', function() {
    var player = new Player("id_09", 0, 4.231, 5, -8.22);
    gameRoom.addPlayer(player);
    const numOfPlayers = gameRoom.getNumOfPlayers();
    assert.equal(numOfPlayers, 1);
  });

  it('getNumOfPlayers() - should get gameRoom players length of 4', function() {
    addTestPlayersToGameRoom();
    const numOfPlayers = gameRoom.getNumOfPlayers();
    assert.equal(numOfPlayers, 4);
  });

  it('doesPlayerExist() - return true for player with id: "id_1"', function() {
    addTestPlayersToGameRoom();
    const doesPlayerExist = gameRoom.doesPlayerExist("id_1");
    assert.equal(doesPlayerExist, true);
  });

  it('doesPlayerExist() - return true for player with id: "id_2"', function() {
    addTestPlayersToGameRoom();
    const doesPlayerExist = gameRoom.doesPlayerExist("id_2");
    assert.equal(doesPlayerExist, true);
  });

  it('doesPlayerExist() - return true for player with id: "id_3"', function() {
    addTestPlayersToGameRoom();
    const doesPlayerExist = gameRoom.doesPlayerExist("id_3");
    assert.equal(doesPlayerExist, true);
  });

  it('doesPlayerExist() - return true for player with id: "id_4"', function() {
    addTestPlayersToGameRoom();
    const doesPlayerExist = gameRoom.doesPlayerExist("id_4");
    assert.equal(doesPlayerExist, true);
  });

  it('doesPlayerExist() - return false for player with id: "id_5"', function() {
    addTestPlayersToGameRoom();
    const doesPlayerExist = gameRoom.doesPlayerExist("id_5");
    assert.equal(doesPlayerExist, false);
  });

  it('doesPlayerExist() - return false for player with id: "id_0"', function() {
    addTestPlayersToGameRoom();
    const doesPlayerExist = gameRoom.doesPlayerExist("id_0");
    assert.equal(doesPlayerExist, false);
  });

  it('clear() - gameRoom.players should be empty list', function() {
    var player = new Player("id_09", 0, 4.231, 5, -8.22);
    gameRoom.addPlayer(player);
    assert.equal(gameRoom.players.length, 1);
    gameRoom.clear();
    assert.equal(gameRoom.players.length, 0);
    assert.deepEqual(gameRoom.players, []);
  });

  it('clear() - gameRoom.players should be empty list', function() {
    addTestPlayersToGameRoom();
    assert.equal(gameRoom.players.length, 4);
    gameRoom.clear();
    assert.equal(gameRoom.players.length, 0);
    assert.deepEqual(gameRoom.players, []);
  });

});

function addTestPlayersToGameRoom() {
  var player = new Player("id_1", 300, 400, 3, 4);
  gameRoom.addPlayer(player);
  var player = new Player("id_2", 400, 500, 4, 5);
  gameRoom.addPlayer(player);
  var player = new Player("id_3", 500, 600, 5, 6);
  gameRoom.addPlayer(player);
  var player = new Player("id_4", 600, 700, 6, 7);
  gameRoom.addPlayer(player);
}
